; minibuffer history
; (list-command-history)
(set 'history-length t) ; def. 100, t is unlimited
(set 'savehist-file "/hist/emacs/minibuffer.hst")
(set 'history-delete-duplicates t)
(savehist-mode 1) ; savehist.el, minibuffer history saving
