; --- man.el ---

(set 'Man-notify-method 'pushy) ; single window (close others)

; creating the single function custom-man-colors
(defun custom-man-colors ()
(set-face-attribute 'Man-overstrike nil :weight 'normal :foreground "cyan") ; nil frame (on all frames)
(set-face-attribute 'Man-underline nil :weight 'normal :foreground "yellow")
)

; adding the function to man mode hook
(add-hook 'Man-mode-hook 'custom-man-colors)

; +-- man.el ---
