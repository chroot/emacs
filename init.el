; --- variables ---

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(set 'make-backup-files nil)




; --- key bindings ---
(global-set-key (kbd "C-h") 'backward-delete-char)
; +-- key bindings ---
; --- minor modes ---
(global-linum-mode)
; +-- minor modes ---

; /etc/emacs directory is in load-path variable
(load "history-mode.el")
(load "man-mode.el")
